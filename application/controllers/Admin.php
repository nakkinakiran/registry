<?php
  class Admin extends CI_Controller{

    // load defaults for the controller
    public function __construct(){
      parent::__construct();
      $this->load->library('session');
      $this->load->helper('form');
      $this->load->model('Admin_model');
    }

    public function index(){ //dashboard
      //global constants that should be everywhere. look for an alternative
      $data['global_styles'] = $this->load->view('common_includes/styles.php', NULL, true);
      $data['global_scripts'] = $this->load->view('common_includes/scripts.php', NULL, true);

      if($this->session->userdata('username')){
        $data['username'] = $this->session->userdata('username');
        $this->load->view('admin/dashboard', $data);
      }
      else{
        redirect('admin/login');
      }

    }

    public function login(){
      //redirect if session is already on
      if($this->session->userdata('username')){
        redirect('admin');
      }
      //global constants that should be everywhere. look for an alternative
      $data['global_styles'] = $this->load->view('common_includes/styles.php', NULL, true);
      $data['global_scripts'] = $this->load->view('common_includes/scripts.php', NULL, true);
      // checking if data is submitted
      if($this->input->post('username')){
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $logged_in = $this->check_admin_creds($username, $password);
        if($logged_in){
          $this->session->set_userdata('username', $username);
          redirect('admin');
        }else{
          redirect('home');
        }
      }
      else{
        redirect('home');
      }

    }

    public function logout(){
      session_destroy();
      redirect('admin');
    }

    public function check_admin_creds($username, $password){
      $creds = $this->Admin_model->get_admin_creds($username, $password);

      $db_username = $creds->comp_username;
      $db_password = $creds->comp_password;
      $password_hash = $password;
      if($username == $db_username){
        if($password == $db_password){
          return true;
        }
        else{
          return false;
        }
      }
      else{
        return false;
      }
    }

  }
?>

<?php
  class Home extends CI_Controller{

    public function __construct(){
      parent::__construct();
      $this->load->library('session');
    }

    public function index(){
      if($this->session->userdata('username')){
        redirect('admin');
      }
      $data['global_styles'] = $this->get_global_styles();
      $data['global_scripts'] = $this->get_global_scripts();
      $this->load->view('index',$data);
    }

    public function get_global_styles(){
      return $styles = $this->load->view('common_includes/styles.php', NULL, true);
    }

    public function get_global_scripts(){
      return $scripts = $this->load->view('common_includes/scripts.php', NULL, true);
    }
  }
?>

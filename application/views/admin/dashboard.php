<!DOCTYPE html>
<html lang="en">
<head>
  <!-- meta -->
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Dashboard</title>
  <!-- styles -->
  <?php echo $global_styles;?>
  <style>
    #sidebar-toggle{
      position:fixed;
      top:15px;
      left:15px;
      z-index: 10;
    }
  </style>
</head>
<body>
  <main>
    <i class="fa fa-ellipsis-h white-text" id="sidebar-toggle"></i>
    <div class="row" style="height:100vh;">
      <div class="col-md-2 indigo" id="sidebar"></div>
      <div class="col-md-10 cyan accent-4" id="main-screen"></div>
    </div>
  </main>
  <!-- scripts -->
  <?php echo $global_scripts;?>
  <script>
    $('#sidebar-toggle').click(function(){
    });
  </script>
</body>
</html>

<a href="<?php echo base_url(); ?>admin/logout">logout</a>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <!-- styles -->
  <?php echo $global_styles;?>
  <style>
    #form-container{
      margin-top:100px;
    }
  </style>
</head>
<body>
  <main>
    <div class="row">
      <div class="col-md-5"></div>
      <div class="col-md-2">
        <div id="form-container" class="shadow p-4">
          <form class="font-weight-bold" id="login-form" action="<?php echo base_url();?>admin/login" method="post">
            <div class="form-group">
              <label for="username">Username</label>
              <input type="text" name="username" id="username" class="form-control" placeholder="Enter your email">
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input type="password" name="password" id="password" class="form-control" placeholder="Enter your password">
            </div>
            <div class="form-group">
              <button class="btn btn-md btn-block bg-info font-weight-bold" type="submit">Login</button>
            </div>
          </form>
        </div>
      </div>

      <div class="col-md-5"></div>
    </div>
  </main>
  <?php echo $global_scripts;?>
</body>
</html>

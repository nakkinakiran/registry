<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Home Page</title>
  <!-- styles -->
  <?php echo $global_styles;?>
  <style>
    body{
      background: url('<?php echo base_url();?>assets/img/homebg.jpg');
      background-repeat:no-repeat;
    }
    main{
      background-color: rgba(71, 106, 135, 0.5);
      display: table;
      position: absolute;
      height:100vh;
      width: 100%;
    }
    .message-container{
      display: table-cell;
      vertical-align: middle;
    }
    .message{
       width:320px;
       text-align:center;
    }
    .title{
      font-size:300%;
    }
  </style>
</head>
<body>
  <header>
    <nav class="navbar navbar-expand-lg navbar-dark unique-color">
      <a href="<?php base_url();?>" class="navbar-brand font-weight-bold">Church Organizer</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#login-form-dropdown" aria-controls="login-form-dropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="login-form-dropdown">
        <form class="form-inline waves-light waves-light ml-auto" method="post" action="<?php echo base_url();?>admin/login">
          <input class="m-2 form-control" name="username" type="text" placeholder="Username" aria-label="Username">
          <input class="m-2 form-control" name="password" type="password" placeholder="Password" aria-label="Password">
          <button class="btn btn-md light-blue darken-4 m-2 waves-light" type="submit">Login</button>
        </form>
      </div>
    </nav>
  </header>

  <main class="text-uppercase white-text">
    <div class="message-container">
      <div class="message ml-auto mr-auto">
        <h1 class="font-weight-bold title">Organizing<br>Your Church</h1>
        <h3 class="font-weight-bold sub-title">Made Easy</h3>
      </div>
    </div>
  </main>

  <!-- scripts -->
  <?php echo $global_scripts;?>
</body>
</html>
